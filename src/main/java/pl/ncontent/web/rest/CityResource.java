package pl.ncontent.web.rest;

import pl.ncontent.service.CityService;
import pl.ncontent.service.dto.AdministrationUnitDTO;
import pl.ncontent.web.rest.errors.BadRequestAlertException;
import pl.ncontent.service.dto.CityDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link pl.ncontent.domain.City}.
 */
@RestController
@RequestMapping("/api")
public class CityResource {

    private final Logger log = LoggerFactory.getLogger(CityResource.class);

    private static final String ENTITY_NAME = "city";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CityService cityService;

    public CityResource(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping("/cities/{provinceName}/{countyName}/{communeName}")
    public ResponseEntity<List<CityDTO>> getCitiesOfGivenProvinceName(@PathVariable String provinceName,
                                                                                    @PathVariable String countyName,
                                                                                    @PathVariable String communeName) {
        log.debug("REST request to get cities list for names:", provinceName, countyName, communeName);
        List<CityDTO> cityDTOList = cityService.getCitiesListInGivenUnitName(provinceName, countyName, communeName);
        return ResponseEntity.ok().body(cityDTOList);
    }
}
