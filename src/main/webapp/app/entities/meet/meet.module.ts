import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MeetShowSharedModule } from 'app/shared';
import {
  MeetComponent,
  MeetDetailComponent,
  MeetUpdateComponent,
  MeetDeletePopupComponent,
  MeetDeleteDialogComponent,
  meetRoute,
  meetPopupRoute
} from './';

const ENTITY_STATES = [...meetRoute, ...meetPopupRoute];

@NgModule({
  imports: [MeetShowSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [MeetComponent, MeetDetailComponent, MeetUpdateComponent, MeetDeleteDialogComponent, MeetDeletePopupComponent],
  entryComponents: [MeetComponent, MeetUpdateComponent, MeetDeleteDialogComponent, MeetDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MeetShowMeetModule {}
