import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IMeetType, MeetType } from 'app/shared/model/meet-type.model';
import { MeetTypeService } from './meet-type.service';

@Component({
  selector: 'jhi-meet-type-update',
  templateUrl: './meet-type-update.component.html'
})
export class MeetTypeUpdateComponent implements OnInit {
  meetType: IMeetType;
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: []
  });

  constructor(protected meetTypeService: MeetTypeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ meetType }) => {
      this.updateForm(meetType);
      this.meetType = meetType;
    });
  }

  updateForm(meetType: IMeetType) {
    this.editForm.patchValue({
      id: meetType.id,
      name: meetType.name
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const meetType = this.createFromForm();
    if (meetType.id !== undefined) {
      this.subscribeToSaveResponse(this.meetTypeService.update(meetType));
    } else {
      this.subscribeToSaveResponse(this.meetTypeService.create(meetType));
    }
  }

  private createFromForm(): IMeetType {
    const entity = {
      ...new MeetType(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMeetType>>) {
    result.subscribe((res: HttpResponse<IMeetType>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
