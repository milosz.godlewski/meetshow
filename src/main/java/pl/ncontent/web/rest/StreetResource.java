package pl.ncontent.web.rest;

import pl.ncontent.service.StreetService;
import pl.ncontent.service.dto.CityDTO;
import pl.ncontent.web.rest.errors.BadRequestAlertException;
import pl.ncontent.service.dto.StreetDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link pl.ncontent.domain.Street}.
 */
@RestController
@RequestMapping("/api")
public class StreetResource {

    private final Logger log = LoggerFactory.getLogger(StreetResource.class);

    private static final String ENTITY_NAME = "street";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StreetService streetService;

    public StreetResource(StreetService streetService) {
        this.streetService = streetService;
    }


    @GetMapping("/streets/{provinceName}/{countyName}/{communeName}/{cityName}")
    public ResponseEntity<List<StreetDTO>> getStreetsOfGivenProvinceName(@PathVariable String provinceName,
                                                                      @PathVariable String countyName,
                                                                      @PathVariable String communeName,
                                                                      @PathVariable String cityName) {
        log.debug("REST request to get streets list for names:", provinceName, countyName, communeName, cityName);
        List<StreetDTO> cityDTOList = streetService.getStreetsListInGivenCityName(
                                provinceName, countyName, communeName, cityName);
        return ResponseEntity.ok().body(cityDTOList);
    }
}
