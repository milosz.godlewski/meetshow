import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { AdministrationUnit } from 'app/shared/model/administration-unit.model';
import { AdministrationUnitService } from './administration-unit.service';
import { AdministrationUnitComponent } from './administration-unit.component';
import { AdministrationUnitDetailComponent } from './administration-unit-detail.component';
import { AdministrationUnitUpdateComponent } from './administration-unit-update.component';
import { AdministrationUnitDeletePopupComponent } from './administration-unit-delete-dialog.component';
import { IAdministrationUnit } from 'app/shared/model/administration-unit.model';

@Injectable({ providedIn: 'root' })
export class AdministrationUnitResolve implements Resolve<IAdministrationUnit> {
  constructor(private service: AdministrationUnitService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAdministrationUnit> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<AdministrationUnit>) => response.ok),
        map((administrationUnit: HttpResponse<AdministrationUnit>) => administrationUnit.body)
      );
    }
    return of(new AdministrationUnit());
  }
}

export const administrationUnitRoute: Routes = [
  {
    path: '',
    component: AdministrationUnitComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'AdministrationUnits'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AdministrationUnitDetailComponent,
    resolve: {
      administrationUnit: AdministrationUnitResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'AdministrationUnits'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AdministrationUnitUpdateComponent,
    resolve: {
      administrationUnit: AdministrationUnitResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'AdministrationUnits'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AdministrationUnitUpdateComponent,
    resolve: {
      administrationUnit: AdministrationUnitResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'AdministrationUnits'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const administrationUnitPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: AdministrationUnitDeletePopupComponent,
    resolve: {
      administrationUnit: AdministrationUnitResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'AdministrationUnits'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
