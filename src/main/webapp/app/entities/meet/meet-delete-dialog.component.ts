import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMeet } from 'app/shared/model/meet.model';
import { MeetService } from './meet.service';

@Component({
  selector: 'jhi-meet-delete-dialog',
  templateUrl: './meet-delete-dialog.component.html'
})
export class MeetDeleteDialogComponent {
  meet: IMeet;

  constructor(protected meetService: MeetService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.meetService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'meetListModification',
        content: 'Deleted an meet'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-meet-delete-popup',
  template: ''
})
export class MeetDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ meet }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MeetDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.meet = meet;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/meet', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/meet', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
