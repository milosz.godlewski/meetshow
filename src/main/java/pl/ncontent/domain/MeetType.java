package pl.ncontent.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A MeetType.
 */
@Entity
@Table(name = "meet_type")
public class MeetType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "meetType")
    private Set<Meet> meets = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public MeetType name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Meet> getMeets() {
        return meets;
    }

    public MeetType meets(Set<Meet> meets) {
        this.meets = meets;
        return this;
    }

    public MeetType addMeet(Meet meet) {
        this.meets.add(meet);
        meet.setMeetType(this);
        return this;
    }

    public MeetType removeMeet(Meet meet) {
        this.meets.remove(meet);
        meet.setMeetType(null);
        return this;
    }

    public void setMeets(Set<Meet> meets) {
        this.meets = meets;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MeetType)) {
            return false;
        }
        return id != null && id.equals(((MeetType) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MeetType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
