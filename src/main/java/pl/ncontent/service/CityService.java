package pl.ncontent.service;

import com.raps.code.generate.ws.SlnError;
import com.raps.code.generate.ws.SlnWebServiceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.ncontent.domain.City;
import pl.ncontent.service.dto.AdministrationUnitDTO;
import pl.ncontent.service.dto.CityDTO;
import pl.ncontent.service.mapper.CityMapper;
import pl.ncontent.web.rest.errors.BadRequestAlertException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link City}.
 */
@Service
public class CityService {

    private final Logger log = LoggerFactory.getLogger(CityService.class);

    private final AdministrationUnitService administrationUnitService;

    private final CityMapper cityMapper;

    public CityService(AdministrationUnitService administrationUnitService, CityMapper cityMapper) {
        this.administrationUnitService = administrationUnitService;
        this.cityMapper = cityMapper;
    }

    public List<CityDTO> getCitiesListInGivenUnitName(String provinceName, String countyName, String communeName) {
        Optional<AdministrationUnitDTO> commune =
            administrationUnitService.getCommuneForGivenName(provinceName, countyName, communeName);
        if (!commune.isPresent()) {
            throw new BadRequestAlertException("Commune name not found", "VALIDATION", "400");
        }
        SlnWebServiceService gugikService = new SlnWebServiceService();
        List<CityDTO> cityListToReturn = new ArrayList<>();
        try {
            gugikService.getSlnWebServicePort()
                .pobierzMiejscowosci(commune.get().getCommunePn(), commune.get().getCommuneId(), false)
                .getMiejscowosc()
                .stream()
                .filter(city -> city.getCyklZyciaDo() == null)          //Removing history records.
                .map(cityMapper::fromWeb)
                .forEach(cityListToReturn::add);
        } catch (SlnError slnError) {
            throw new BadRequestAlertException("Error", "ERROR", "500");
        }
        return cityListToReturn;
    }
}
