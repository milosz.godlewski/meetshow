import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MeetShowSharedModule } from 'app/shared';
import {
  CountyComponent,
  CountyDetailComponent,
  CountyUpdateComponent,
  CountyDeletePopupComponent,
  CountyDeleteDialogComponent,
  countyRoute,
  countyPopupRoute
} from './';

const ENTITY_STATES = [...countyRoute, ...countyPopupRoute];

@NgModule({
  imports: [MeetShowSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [CountyComponent, CountyDetailComponent, CountyUpdateComponent, CountyDeleteDialogComponent, CountyDeletePopupComponent],
  entryComponents: [CountyComponent, CountyUpdateComponent, CountyDeleteDialogComponent, CountyDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MeetShowCountyModule {}
