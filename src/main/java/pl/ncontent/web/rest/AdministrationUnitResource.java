package pl.ncontent.web.rest;

import pl.ncontent.service.AdministrationUnitService;
import pl.ncontent.service.dto.AdministrationUnitDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST controller for managing {@link pl.ncontent.domain.AdministrationUnit}.
 */
@RestController
@RequestMapping("/api")
public class AdministrationUnitResource {

    private final Logger log = LoggerFactory.getLogger(AdministrationUnitResource.class);

    private static final String ENTITY_NAME = "administrationUnit";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AdministrationUnitService administrationUnitService;

    public AdministrationUnitResource(AdministrationUnitService administrationUnitService) {
        this.administrationUnitService = administrationUnitService;
    }


    /**
     * {@code GET  /counties/west.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the AdministrationUnitDTO,
     * or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/counties/west")
    public ResponseEntity<List<AdministrationUnitDTO>> getWestPomeranianCounties() {
        log.debug("REST request to get counties list in west pomeranian.");
        List<AdministrationUnitDTO> countyDTO = administrationUnitService.getWestPomeranianCounties();
        return ResponseEntity.ok().body(countyDTO);
    }

    @GetMapping("/counties/{provinceName}/")
    public ResponseEntity<List<AdministrationUnitDTO>> getCountiesOfGivenProvinceName(@PathVariable String provinceName) {
        log.debug("REST request to get cities list for name:", provinceName);
        List<AdministrationUnitDTO> countyDTO = administrationUnitService.getCountiesList(provinceName);
        return ResponseEntity.ok().body(countyDTO);
    }

    @GetMapping("/communes/{provinceName}/{countyName}/")
    public ResponseEntity<List<AdministrationUnitDTO>> getCommunesOfGivenCountyName(@PathVariable String provinceName,
                                                                                    @PathVariable String countyName) {
        log.debug("REST request to get cities list in west pomeranian.");
        List<AdministrationUnitDTO> countyDTO = administrationUnitService.getCommunesForGivenCountyName(provinceName, countyName);
        return ResponseEntity.ok().body(countyDTO);
    }

    @GetMapping("/communes/police/")
    public ResponseEntity getCommunesOfPolice() {
        log.debug("REST request to get communes list for Police:");
        return ResponseEntity.ok().build();
    }


}
