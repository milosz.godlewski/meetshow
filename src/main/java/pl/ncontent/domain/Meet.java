package pl.ncontent.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Meet.
 */
@Entity
@Table(name = "meet")
public class Meet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "starting_date", nullable = false)
    private ZonedDateTime startingDate;

    @NotNull
    @Column(name = "ending_date", nullable = false)
    private ZonedDateTime endingDate;

    @ManyToOne
    @JsonIgnoreProperties("meets")
    private Priority priority;

    @ManyToOne
    @JsonIgnoreProperties("meets")
    private MeetType meetType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getStartingDate() {
        return startingDate;
    }

    public Meet startingDate(ZonedDateTime startingDate) {
        this.startingDate = startingDate;
        return this;
    }

    public void setStartingDate(ZonedDateTime startingDate) {
        this.startingDate = startingDate;
    }

    public ZonedDateTime getEndingDate() {
        return endingDate;
    }

    public Meet endingDate(ZonedDateTime endingDate) {
        this.endingDate = endingDate;
        return this;
    }

    public void setEndingDate(ZonedDateTime endingDate) {
        this.endingDate = endingDate;
    }

    public Priority getPriority() {
        return priority;
    }

    public Meet priority(Priority priority) {
        this.priority = priority;
        return this;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public MeetType getMeetType() {
        return meetType;
    }

    public Meet meetType(MeetType meetType) {
        this.meetType = meetType;
        return this;
    }

    public void setMeetType(MeetType meetType) {
        this.meetType = meetType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Meet)) {
            return false;
        }
        return id != null && id.equals(((Meet) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Meet{" +
            "id=" + getId() +
            ", startingDate='" + getStartingDate() + "'" +
            ", endingDate='" + getEndingDate() + "'" +
            "}";
    }
}
