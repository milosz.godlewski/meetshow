package pl.ncontent.web.rest;

import pl.ncontent.MeetShowApp;
import pl.ncontent.domain.AdministrationUnit;
import pl.ncontent.repository.AdministrationUnitRepository;
import pl.ncontent.service.AdministrationUnitService;
import pl.ncontent.service.dto.AdministrationUnitDTO;
import pl.ncontent.service.mapper.AdministrationUnitMapper;
import pl.ncontent.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static pl.ncontent.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link AdministrationUnitResource} REST controller.
 */
@SpringBootTest(classes = MeetShowApp.class)
public class AdministrationUnitResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private AdministrationUnitRepository administrationUnitRepository;

    @Autowired
    private AdministrationUnitMapper administrationUnitMapper;

    @Autowired
    private AdministrationUnitService administrationUnitService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAdministrationUnitMockMvc;

    private AdministrationUnit administrationUnit;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AdministrationUnitResource administrationUnitResource = new AdministrationUnitResource(administrationUnitService);
        this.restAdministrationUnitMockMvc = MockMvcBuilders.standaloneSetup(administrationUnitResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdministrationUnit createEntity(EntityManager em) {
        AdministrationUnit administrationUnit = new AdministrationUnit()
            .name(DEFAULT_NAME);
        return administrationUnit;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdministrationUnit createUpdatedEntity(EntityManager em) {
        AdministrationUnit administrationUnit = new AdministrationUnit()
            .name(UPDATED_NAME);
        return administrationUnit;
    }

    @BeforeEach
    public void initTest() {
        administrationUnit = createEntity(em);
    }

}
