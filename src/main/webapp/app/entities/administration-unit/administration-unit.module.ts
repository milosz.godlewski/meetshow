import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MeetShowSharedModule } from 'app/shared';
import {
  AdministrationUnitComponent,
  AdministrationUnitDetailComponent,
  AdministrationUnitUpdateComponent,
  AdministrationUnitDeletePopupComponent,
  AdministrationUnitDeleteDialogComponent,
  administrationUnitRoute,
  administrationUnitPopupRoute
} from './';

const ENTITY_STATES = [...administrationUnitRoute, ...administrationUnitPopupRoute];

@NgModule({
  imports: [MeetShowSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    AdministrationUnitComponent,
    AdministrationUnitDetailComponent,
    AdministrationUnitUpdateComponent,
    AdministrationUnitDeleteDialogComponent,
    AdministrationUnitDeletePopupComponent
  ],
  entryComponents: [
    AdministrationUnitComponent,
    AdministrationUnitUpdateComponent,
    AdministrationUnitDeleteDialogComponent,
    AdministrationUnitDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MeetShowAdministrationUnitModule {}
