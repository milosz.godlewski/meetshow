package pl.ncontent.web.rest;

import pl.ncontent.MeetShowApp;
import pl.ncontent.domain.Meet;
import pl.ncontent.repository.MeetRepository;
import pl.ncontent.service.MeetService;
import pl.ncontent.service.dto.MeetDTO;
import pl.ncontent.service.mapper.MeetMapper;
import pl.ncontent.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static pl.ncontent.web.rest.TestUtil.sameInstant;
import static pl.ncontent.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link MeetResource} REST controller.
 */
@SpringBootTest(classes = MeetShowApp.class)
public class MeetResourceIT {

    private static final ZonedDateTime DEFAULT_STARTING_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_STARTING_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_ENDING_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_ENDING_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private MeetRepository meetRepository;

    @Autowired
    private MeetMapper meetMapper;

    @Autowired
    private MeetService meetService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMeetMockMvc;

    private Meet meet;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MeetResource meetResource = new MeetResource(meetService);
        this.restMeetMockMvc = MockMvcBuilders.standaloneSetup(meetResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Meet createEntity(EntityManager em) {
        Meet meet = new Meet()
            .startingDate(DEFAULT_STARTING_DATE)
            .endingDate(DEFAULT_ENDING_DATE);
        return meet;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Meet createUpdatedEntity(EntityManager em) {
        Meet meet = new Meet()
            .startingDate(UPDATED_STARTING_DATE)
            .endingDate(UPDATED_ENDING_DATE);
        return meet;
    }

    @BeforeEach
    public void initTest() {
        meet = createEntity(em);
    }

    @Test
    @Transactional
    public void createMeet() throws Exception {
        int databaseSizeBeforeCreate = meetRepository.findAll().size();

        // Create the Meet
        MeetDTO meetDTO = meetMapper.toDto(meet);
        restMeetMockMvc.perform(post("/api/meets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(meetDTO)))
            .andExpect(status().isCreated());

        // Validate the Meet in the database
        List<Meet> meetList = meetRepository.findAll();
        assertThat(meetList).hasSize(databaseSizeBeforeCreate + 1);
        Meet testMeet = meetList.get(meetList.size() - 1);
        assertThat(testMeet.getStartingDate()).isEqualTo(DEFAULT_STARTING_DATE);
        assertThat(testMeet.getEndingDate()).isEqualTo(DEFAULT_ENDING_DATE);
    }

    @Test
    public void isDateAvailableTest() throws Exception {
        int databaseSizeBeforeCreate = meetRepository.findAll().size();

        // Create the Meet
        MeetDTO meetDTO = meetMapper.toDto(meet);
        restMeetMockMvc.perform(post("/api/meets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(meetDTO)))
            .andExpect(status().isCreated());

        restMeetMockMvc.perform(post("/api/meets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(meetDTO)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void createMeetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = meetRepository.findAll().size();

        // Create the Meet with an existing ID
        meet.setId(1L);
        MeetDTO meetDTO = meetMapper.toDto(meet);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMeetMockMvc.perform(post("/api/meets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(meetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Meet in the database
        List<Meet> meetList = meetRepository.findAll();
        assertThat(meetList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkStartingDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = meetRepository.findAll().size();
        // set the field null
        meet.setStartingDate(null);

        // Create the Meet, which fails.
        MeetDTO meetDTO = meetMapper.toDto(meet);

        restMeetMockMvc.perform(post("/api/meets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(meetDTO)))
            .andExpect(status().isBadRequest());

        List<Meet> meetList = meetRepository.findAll();
        assertThat(meetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEndingDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = meetRepository.findAll().size();
        // set the field null
        meet.setEndingDate(null);

        // Create the Meet, which fails.
        MeetDTO meetDTO = meetMapper.toDto(meet);

        restMeetMockMvc.perform(post("/api/meets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(meetDTO)))
            .andExpect(status().isBadRequest());

        List<Meet> meetList = meetRepository.findAll();
        assertThat(meetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMeets() throws Exception {
        // Initialize the database
        meetRepository.saveAndFlush(meet);

        // Get all the meetList
        restMeetMockMvc.perform(get("/api/meets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(meet.getId().intValue())))
            .andExpect(jsonPath("$.[*].startingDate").value(hasItem(sameInstant(DEFAULT_STARTING_DATE))))
            .andExpect(jsonPath("$.[*].endingDate").value(hasItem(sameInstant(DEFAULT_ENDING_DATE))));
    }
    
    @Test
    @Transactional
    public void getMeet() throws Exception {
        // Initialize the database
        meetRepository.saveAndFlush(meet);

        // Get the meet
        restMeetMockMvc.perform(get("/api/meets/{id}", meet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(meet.getId().intValue()))
            .andExpect(jsonPath("$.startingDate").value(sameInstant(DEFAULT_STARTING_DATE)))
            .andExpect(jsonPath("$.endingDate").value(sameInstant(DEFAULT_ENDING_DATE)));
    }

    @Test
    @Transactional
    public void getNonExistingMeet() throws Exception {
        // Get the meet
        restMeetMockMvc.perform(get("/api/meets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMeet() throws Exception {
        // Initialize the database
        meetRepository.saveAndFlush(meet);

        int databaseSizeBeforeUpdate = meetRepository.findAll().size();

        // Update the meet
        Meet updatedMeet = meetRepository.findById(meet.getId()).get();
        // Disconnect from session so that the updates on updatedMeet are not directly saved in db
        em.detach(updatedMeet);
        updatedMeet
            .startingDate(UPDATED_STARTING_DATE)
            .endingDate(UPDATED_ENDING_DATE);
        MeetDTO meetDTO = meetMapper.toDto(updatedMeet);

        restMeetMockMvc.perform(put("/api/meets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(meetDTO)))
            .andExpect(status().isOk());

        // Validate the Meet in the database
        List<Meet> meetList = meetRepository.findAll();
        assertThat(meetList).hasSize(databaseSizeBeforeUpdate);
        Meet testMeet = meetList.get(meetList.size() - 1);
        assertThat(testMeet.getStartingDate()).isEqualTo(UPDATED_STARTING_DATE);
        assertThat(testMeet.getEndingDate()).isEqualTo(UPDATED_ENDING_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingMeet() throws Exception {
        int databaseSizeBeforeUpdate = meetRepository.findAll().size();

        // Create the Meet
        MeetDTO meetDTO = meetMapper.toDto(meet);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMeetMockMvc.perform(put("/api/meets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(meetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Meet in the database
        List<Meet> meetList = meetRepository.findAll();
        assertThat(meetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMeet() throws Exception {
        // Initialize the database
        meetRepository.saveAndFlush(meet);

        int databaseSizeBeforeDelete = meetRepository.findAll().size();

        // Delete the meet
        restMeetMockMvc.perform(delete("/api/meets/{id}", meet.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Meet> meetList = meetRepository.findAll();
        assertThat(meetList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Meet.class);
        Meet meet1 = new Meet();
        meet1.setId(1L);
        Meet meet2 = new Meet();
        meet2.setId(meet1.getId());
        assertThat(meet1).isEqualTo(meet2);
        meet2.setId(2L);
        assertThat(meet1).isNotEqualTo(meet2);
        meet1.setId(null);
        assertThat(meet1).isNotEqualTo(meet2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MeetDTO.class);
        MeetDTO meetDTO1 = new MeetDTO();
        meetDTO1.setId(1L);
        MeetDTO meetDTO2 = new MeetDTO();
        assertThat(meetDTO1).isNotEqualTo(meetDTO2);
        meetDTO2.setId(meetDTO1.getId());
        assertThat(meetDTO1).isEqualTo(meetDTO2);
        meetDTO2.setId(2L);
        assertThat(meetDTO1).isNotEqualTo(meetDTO2);
        meetDTO1.setId(null);
        assertThat(meetDTO1).isNotEqualTo(meetDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(meetMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(meetMapper.fromId(null)).isNull();
    }
}
