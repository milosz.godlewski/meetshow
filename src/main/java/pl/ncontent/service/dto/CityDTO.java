package pl.ncontent.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link pl.ncontent.domain.City} entity.
 */
public class CityDTO implements Serializable {

    private Long id;

    private String name;
    private String cityId;
    private String cityPn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityPn() {
        return cityPn;
    }

    public void setCityPn(String cityPn) {
        this.cityPn = cityPn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CityDTO cityDTO = (CityDTO) o;
        return Objects.equals(id, cityDTO.id) &&
            Objects.equals(name, cityDTO.name) &&
            Objects.equals(cityId, cityDTO.cityId) &&
            Objects.equals(cityPn, cityDTO.cityPn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, cityId, cityPn);
    }

    @Override
    public String toString() {
        return "CityDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", cityId='" + cityId + '\'' +
            ", cityPn='" + cityPn + '\'' +
            '}';
    }
}
