import { IMeet } from 'app/shared/model/meet.model';

export interface IPriority {
  id?: number;
  name?: string;
  meets?: IMeet[];
}

export class Priority implements IPriority {
  constructor(public id?: number, public name?: string, public meets?: IMeet[]) {}
}
