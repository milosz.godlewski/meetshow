package pl.ncontent.service.dto;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link pl.ncontent.domain.Meet} entity.
 */
public class MeetDTO implements Serializable {

    private Long id;

    @NotNull
    private ZonedDateTime startingDate;

    @NotNull
    private ZonedDateTime endingDate;


    private Long priorityId;

    private Long meetTypeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(ZonedDateTime startingDate) {
        this.startingDate = startingDate;
    }

    public ZonedDateTime getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(ZonedDateTime endingDate) {
        this.endingDate = endingDate;
    }

    public Long getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(Long priorityId) {
        this.priorityId = priorityId;
    }

    public Long getMeetTypeId() {
        return meetTypeId;
    }

    public void setMeetTypeId(Long meetTypeId) {
        this.meetTypeId = meetTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MeetDTO meetDTO = (MeetDTO) o;
        if (meetDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), meetDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MeetDTO{" +
            "id=" + getId() +
            ", startingDate='" + getStartingDate() + "'" +
            ", endingDate='" + getEndingDate() + "'" +
            ", priority=" + getPriorityId() +
            ", meetType=" + getMeetTypeId() +
            "}";
    }
}
