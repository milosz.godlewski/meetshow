import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAdministrationUnit } from 'app/shared/model/administration-unit.model';
import { AdministrationUnitService } from './administration-unit.service';

@Component({
  selector: 'jhi-administration-unit-delete-dialog',
  templateUrl: './administration-unit-delete-dialog.component.html'
})
export class AdministrationUnitDeleteDialogComponent {
  administrationUnit: IAdministrationUnit;

  constructor(
    protected administrationUnitService: AdministrationUnitService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.administrationUnitService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'administrationUnitListModification',
        content: 'Deleted an administrationUnit'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-administration-unit-delete-popup',
  template: ''
})
export class AdministrationUnitDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ administrationUnit }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(AdministrationUnitDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.administrationUnit = administrationUnit;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/administration-unit', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/administration-unit', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
