import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMeetType } from 'app/shared/model/meet-type.model';
import { MeetTypeService } from './meet-type.service';

@Component({
  selector: 'jhi-meet-type-delete-dialog',
  templateUrl: './meet-type-delete-dialog.component.html'
})
export class MeetTypeDeleteDialogComponent {
  meetType: IMeetType;

  constructor(protected meetTypeService: MeetTypeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.meetTypeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'meetTypeListModification',
        content: 'Deleted an meetType'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-meet-type-delete-popup',
  template: ''
})
export class MeetTypeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ meetType }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MeetTypeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.meetType = meetType;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/meet-type', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/meet-type', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
