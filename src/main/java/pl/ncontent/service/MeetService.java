package pl.ncontent.service;

import io.undertow.util.BadRequestException;
import pl.ncontent.domain.Meet;
import pl.ncontent.repository.MeetRepository;
import pl.ncontent.service.dto.MeetDTO;
import pl.ncontent.service.mapper.MeetMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.ncontent.web.rest.errors.BadRequestAlertException;

import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Meet}.
 */
@Service
@Transactional
public class MeetService {

    private final Logger log = LoggerFactory.getLogger(MeetService.class);

    private final MeetRepository meetRepository;

    private final MeetMapper meetMapper;

    public MeetService(MeetRepository meetRepository, MeetMapper meetMapper) {
        this.meetRepository = meetRepository;
        this.meetMapper = meetMapper;
    }

    /**
     * Save a meet.
     *
     * @param meetDTO the entity to save.
     * @return the persisted entity.
     */
    public MeetDTO save(MeetDTO meetDTO) {
        log.debug("Request to save Meet : {}", meetDTO);
        Meet meet = meetMapper.toEntity(meetDTO);
        if (!isDateAvailable(meet.getStartingDate(), meet.getEndingDate())) {
            throw new BadRequestAlertException("Already booked in given date.", "VALIDATION", "400");
        }
        meet = meetRepository.save(meet);
        return meetMapper.toDto(meet);
    }

    /**
     * Checks the date availability.
     *
     * @param startingDate
     * @param endingDate
     * @return boolean.
     */
    public boolean isDateAvailable(ZonedDateTime startingDate, ZonedDateTime endingDate) {
        return meetRepository.getNumberOfMeetingsBetweenDates(startingDate, endingDate) == 0;
    }

    /**
     * Get all the meets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MeetDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Meets");
        return meetRepository.findAll(pageable)
            .map(meetMapper::toDto);
    }


    /**
     * Get one meet by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MeetDTO> findOne(Long id) {
        log.debug("Request to get Meet : {}", id);
        return meetRepository.findById(id)
            .map(meetMapper::toDto);
    }

    /**
     * Delete the meet by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Meet : {}", id);
        meetRepository.deleteById(id);
    }
}
