import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Meet } from 'app/shared/model/meet.model';
import { MeetService } from './meet.service';
import { MeetComponent } from './meet.component';
import { MeetDetailComponent } from './meet-detail.component';
import { MeetUpdateComponent } from './meet-update.component';
import { MeetDeletePopupComponent } from './meet-delete-dialog.component';
import { IMeet } from 'app/shared/model/meet.model';

@Injectable({ providedIn: 'root' })
export class MeetResolve implements Resolve<IMeet> {
  constructor(private service: MeetService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMeet> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Meet>) => response.ok),
        map((meet: HttpResponse<Meet>) => meet.body)
      );
    }
    return of(new Meet());
  }
}

export const meetRoute: Routes = [
  {
    path: '',
    component: MeetComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Meets'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MeetDetailComponent,
    resolve: {
      meet: MeetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Meets'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MeetUpdateComponent,
    resolve: {
      meet: MeetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Meets'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MeetUpdateComponent,
    resolve: {
      meet: MeetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Meets'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const meetPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MeetDeletePopupComponent,
    resolve: {
      meet: MeetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Meets'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
