import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAdministrationUnit } from 'app/shared/model/administration-unit.model';

type EntityResponseType = HttpResponse<IAdministrationUnit>;
type EntityArrayResponseType = HttpResponse<IAdministrationUnit[]>;

@Injectable({ providedIn: 'root' })
export class AdministrationUnitService {
  public resourceUrl = SERVER_API_URL + 'api/administration-units';

  constructor(protected http: HttpClient) {}

  create(administrationUnit: IAdministrationUnit): Observable<EntityResponseType> {
    return this.http.post<IAdministrationUnit>(this.resourceUrl, administrationUnit, { observe: 'response' });
  }

  update(administrationUnit: IAdministrationUnit): Observable<EntityResponseType> {
    return this.http.put<IAdministrationUnit>(this.resourceUrl, administrationUnit, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAdministrationUnit>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAdministrationUnit[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
