package pl.ncontent.service.mapper;

import pl.ncontent.domain.*;
import pl.ncontent.service.dto.MeetDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Meet} and its DTO {@link MeetDTO}.
 */
@Mapper(componentModel = "spring", uses = {PriorityMapper.class, MeetTypeMapper.class})
public interface MeetMapper extends EntityMapper<MeetDTO, Meet> {

    @Mapping(source = "priority.id", target = "priorityId")
    @Mapping(source = "meetType.id", target = "meetTypeId")
    MeetDTO toDto(Meet meet);

    @Mapping(source = "priorityId", target = "priority")
    @Mapping(source = "meetTypeId", target = "meetType")
    Meet toEntity(MeetDTO meetDTO);

    default Meet fromId(Long id) {
        if (id == null) {
            return null;
        }
        Meet meet = new Meet();
        meet.setId(id);
        return meet;
    }
}
