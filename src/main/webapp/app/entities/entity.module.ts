import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'meet',
        loadChildren: './meet/meet.module#MeetShowMeetModule'
      },
      {
        path: 'priority',
        loadChildren: './priority/priority.module#MeetShowPriorityModule'
      },
      {
        path: 'meet-type',
        loadChildren: './meet-type/meet-type.module#MeetShowMeetTypeModule'
      },
      {
        path: 'province',
        loadChildren: './province/province.module#MeetShowProvinceModule'
      },
      {
        path: 'county',
        loadChildren: './county/county.module#MeetShowCountyModule'
      },
      {
        path: 'commune',
        loadChildren: './commune/commune.module#MeetShowCommuneModule'
      },
      {
        path: 'administration-unit',
        loadChildren: './administration-unit/administration-unit.module#MeetShowAdministrationUnitModule'
      },
      {
        path: 'city',
        loadChildren: './city/city.module#MeetShowCityModule'
      },
      {
        path: 'street',
        loadChildren: './street/street.module#MeetShowStreetModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MeetShowEntityModule {}
