package pl.ncontent.service.mapper;

import com.raps.code.generate.ws.JednAdm;
import pl.ncontent.domain.*;
import pl.ncontent.service.dto.AdministrationUnitDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link AdministrationUnit} and its DTO {@link AdministrationUnitDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdministrationUnitMapper extends EntityMapper<AdministrationUnitDTO, AdministrationUnit> {

    default AdministrationUnitDTO fromWeb(JednAdm webUnit) {
        AdministrationUnitDTO unit = new AdministrationUnitDTO();
        switch (webUnit.getPoziom()) {
            case 2:
                unit.setName(webUnit.getWojNazwa());
                unit.setProvinceId(webUnit.getWojIIPId());
                unit.setProvincePn(webUnit.getWojIIPPn());
                break;
            case 3:
                unit.setName(webUnit.getPowNazwa());
                unit.setProvinceId(webUnit.getWojIIPId());
                unit.setProvincePn(webUnit.getWojIIPPn());
                unit.setCountyId(webUnit.getPowIIPId());
                unit.setCountyPn(webUnit.getPowIIPPn());
                break;
            case 4:
                unit.setName(webUnit.getGmNazwa());
                unit.setProvinceId(webUnit.getWojIIPId());
                unit.setProvincePn(webUnit.getWojIIPPn());
                unit.setCountyId(webUnit.getPowIIPId());
                unit.setCountyPn(webUnit.getPowIIPPn());
                unit.setCommuneId(webUnit.getGmIIPId());
                unit.setCommunePn(webUnit.getGmIIPPn());
                break;
            default:
                break;
        }
        return unit;
    }

    default AdministrationUnit fromId(Long id) {
        if (id == null) {
            return null;
        }
        AdministrationUnit administrationUnit = new AdministrationUnit();
        administrationUnit.setId(id);
        return administrationUnit;
    }
}
