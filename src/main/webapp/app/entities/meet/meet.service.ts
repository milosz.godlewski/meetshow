import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMeet } from 'app/shared/model/meet.model';

type EntityResponseType = HttpResponse<IMeet>;
type EntityArrayResponseType = HttpResponse<IMeet[]>;

@Injectable({ providedIn: 'root' })
export class MeetService {
  public resourceUrl = SERVER_API_URL + 'api/meets';

  constructor(protected http: HttpClient) {}

  create(meet: IMeet): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(meet);
    return this.http
      .post<IMeet>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(meet: IMeet): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(meet);
    return this.http
      .put<IMeet>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMeet>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMeet[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(meet: IMeet): IMeet {
    const copy: IMeet = Object.assign({}, meet, {
      startingDate: meet.startingDate != null && meet.startingDate.isValid() ? meet.startingDate.toJSON() : null,
      endingDate: meet.endingDate != null && meet.endingDate.isValid() ? meet.endingDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startingDate = res.body.startingDate != null ? moment(res.body.startingDate) : null;
      res.body.endingDate = res.body.endingDate != null ? moment(res.body.endingDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((meet: IMeet) => {
        meet.startingDate = meet.startingDate != null ? moment(meet.startingDate) : null;
        meet.endingDate = meet.endingDate != null ? moment(meet.endingDate) : null;
      });
    }
    return res;
  }
}
