import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMeetType } from 'app/shared/model/meet-type.model';

type EntityResponseType = HttpResponse<IMeetType>;
type EntityArrayResponseType = HttpResponse<IMeetType[]>;

@Injectable({ providedIn: 'root' })
export class MeetTypeService {
  public resourceUrl = SERVER_API_URL + 'api/meet-types';

  constructor(protected http: HttpClient) {}

  create(meetType: IMeetType): Observable<EntityResponseType> {
    return this.http.post<IMeetType>(this.resourceUrl, meetType, { observe: 'response' });
  }

  update(meetType: IMeetType): Observable<EntityResponseType> {
    return this.http.put<IMeetType>(this.resourceUrl, meetType, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMeetType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMeetType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
