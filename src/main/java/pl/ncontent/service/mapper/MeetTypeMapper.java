package pl.ncontent.service.mapper;

import pl.ncontent.domain.*;
import pl.ncontent.service.dto.MeetTypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link MeetType} and its DTO {@link MeetTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MeetTypeMapper extends EntityMapper<MeetTypeDTO, MeetType> {


    @Mapping(target = "meets", ignore = true)
    MeetType toEntity(MeetTypeDTO meetTypeDTO);

    default MeetType fromId(Long id) {
        if (id == null) {
            return null;
        }
        MeetType meetType = new MeetType();
        meetType.setId(id);
        return meetType;
    }
}
