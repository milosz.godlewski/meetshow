package pl.ncontent.repository;

import pl.ncontent.domain.AdministrationUnit;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AdministrationUnit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdministrationUnitRepository extends JpaRepository<AdministrationUnit, Long> {

}
