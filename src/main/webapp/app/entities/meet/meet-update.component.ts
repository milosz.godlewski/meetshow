import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IMeet, Meet } from 'app/shared/model/meet.model';
import { MeetService } from './meet.service';
import { IPriority } from 'app/shared/model/priority.model';
import { PriorityService } from 'app/entities/priority';
import { IMeetType } from 'app/shared/model/meet-type.model';
import { MeetTypeService } from 'app/entities/meet-type';

@Component({
  selector: 'jhi-meet-update',
  templateUrl: './meet-update.component.html'
})
export class MeetUpdateComponent implements OnInit {
  meet: IMeet;
  isSaving: boolean;

  priorities: IPriority[];

  meettypes: IMeetType[];

  editForm = this.fb.group({
    id: [],
    startingDate: [null, [Validators.required]],
    endingDate: [null, [Validators.required]],
    priorityId: [],
    meetTypeId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected meetService: MeetService,
    protected priorityService: PriorityService,
    protected meetTypeService: MeetTypeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ meet }) => {
      this.updateForm(meet);
      this.meet = meet;
    });
    this.priorityService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IPriority[]>) => mayBeOk.ok),
        map((response: HttpResponse<IPriority[]>) => response.body)
      )
      .subscribe((res: IPriority[]) => (this.priorities = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.meetTypeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMeetType[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMeetType[]>) => response.body)
      )
      .subscribe((res: IMeetType[]) => (this.meettypes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(meet: IMeet) {
    this.editForm.patchValue({
      id: meet.id,
      startingDate: meet.startingDate != null ? meet.startingDate.format(DATE_TIME_FORMAT) : null,
      endingDate: meet.endingDate != null ? meet.endingDate.format(DATE_TIME_FORMAT) : null,
      priorityId: meet.priorityId,
      meetTypeId: meet.meetTypeId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const meet = this.createFromForm();
    if (meet.id !== undefined) {
      this.subscribeToSaveResponse(this.meetService.update(meet));
    } else {
      this.subscribeToSaveResponse(this.meetService.create(meet));
    }
  }

  private createFromForm(): IMeet {
    const entity = {
      ...new Meet(),
      id: this.editForm.get(['id']).value,
      startingDate:
        this.editForm.get(['startingDate']).value != null ? moment(this.editForm.get(['startingDate']).value, DATE_TIME_FORMAT) : undefined,
      endingDate:
        this.editForm.get(['endingDate']).value != null ? moment(this.editForm.get(['endingDate']).value, DATE_TIME_FORMAT) : undefined,
      priorityId: this.editForm.get(['priorityId']).value,
      meetTypeId: this.editForm.get(['meetTypeId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMeet>>) {
    result.subscribe((res: HttpResponse<IMeet>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackPriorityById(index: number, item: IPriority) {
    return item.id;
  }

  trackMeetTypeById(index: number, item: IMeetType) {
    return item.id;
  }
}
