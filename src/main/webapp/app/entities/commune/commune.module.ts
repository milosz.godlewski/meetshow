import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MeetShowSharedModule } from 'app/shared';
import {
  CommuneComponent,
  CommuneDetailComponent,
  CommuneUpdateComponent,
  CommuneDeletePopupComponent,
  CommuneDeleteDialogComponent,
  communeRoute,
  communePopupRoute
} from './';

const ENTITY_STATES = [...communeRoute, ...communePopupRoute];

@NgModule({
  imports: [MeetShowSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CommuneComponent,
    CommuneDetailComponent,
    CommuneUpdateComponent,
    CommuneDeleteDialogComponent,
    CommuneDeletePopupComponent
  ],
  entryComponents: [CommuneComponent, CommuneUpdateComponent, CommuneDeleteDialogComponent, CommuneDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MeetShowCommuneModule {}
