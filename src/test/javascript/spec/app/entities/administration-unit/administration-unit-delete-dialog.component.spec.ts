/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MeetShowTestModule } from '../../../test.module';
import { AdministrationUnitDeleteDialogComponent } from 'app/entities/administration-unit/administration-unit-delete-dialog.component';
import { AdministrationUnitService } from 'app/entities/administration-unit/administration-unit.service';

describe('Component Tests', () => {
  describe('AdministrationUnit Management Delete Component', () => {
    let comp: AdministrationUnitDeleteDialogComponent;
    let fixture: ComponentFixture<AdministrationUnitDeleteDialogComponent>;
    let service: AdministrationUnitService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MeetShowTestModule],
        declarations: [AdministrationUnitDeleteDialogComponent]
      })
        .overrideTemplate(AdministrationUnitDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AdministrationUnitDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AdministrationUnitService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
