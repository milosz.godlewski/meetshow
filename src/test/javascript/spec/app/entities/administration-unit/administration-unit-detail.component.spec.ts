/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MeetShowTestModule } from '../../../test.module';
import { AdministrationUnitDetailComponent } from 'app/entities/administration-unit/administration-unit-detail.component';
import { AdministrationUnit } from 'app/shared/model/administration-unit.model';

describe('Component Tests', () => {
  describe('AdministrationUnit Management Detail Component', () => {
    let comp: AdministrationUnitDetailComponent;
    let fixture: ComponentFixture<AdministrationUnitDetailComponent>;
    const route = ({ data: of({ administrationUnit: new AdministrationUnit(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MeetShowTestModule],
        declarations: [AdministrationUnitDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AdministrationUnitDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AdministrationUnitDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.administrationUnit).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
