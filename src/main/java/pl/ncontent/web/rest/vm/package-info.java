/**
 * View Models used by Spring MVC REST controllers.
 */
package pl.ncontent.web.rest.vm;
