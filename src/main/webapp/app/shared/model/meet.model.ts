import { Moment } from 'moment';

export interface IMeet {
  id?: number;
  startingDate?: Moment;
  endingDate?: Moment;
  priorityId?: number;
  meetTypeId?: number;
}

export class Meet implements IMeet {
  constructor(
    public id?: number,
    public startingDate?: Moment,
    public endingDate?: Moment,
    public priorityId?: number,
    public meetTypeId?: number
  ) {}
}
