import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MeetShowSharedLibsModule, MeetShowSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [MeetShowSharedLibsModule, MeetShowSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [MeetShowSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MeetShowSharedModule {
  static forRoot() {
    return {
      ngModule: MeetShowSharedModule
    };
  }
}
