export interface ICommune {
  id?: number;
  name?: string;
}

export class Commune implements ICommune {
  constructor(public id?: number, public name?: string) {}
}
