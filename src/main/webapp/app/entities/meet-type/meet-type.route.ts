import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MeetType } from 'app/shared/model/meet-type.model';
import { MeetTypeService } from './meet-type.service';
import { MeetTypeComponent } from './meet-type.component';
import { MeetTypeDetailComponent } from './meet-type-detail.component';
import { MeetTypeUpdateComponent } from './meet-type-update.component';
import { MeetTypeDeletePopupComponent } from './meet-type-delete-dialog.component';
import { IMeetType } from 'app/shared/model/meet-type.model';

@Injectable({ providedIn: 'root' })
export class MeetTypeResolve implements Resolve<IMeetType> {
  constructor(private service: MeetTypeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMeetType> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<MeetType>) => response.ok),
        map((meetType: HttpResponse<MeetType>) => meetType.body)
      );
    }
    return of(new MeetType());
  }
}

export const meetTypeRoute: Routes = [
  {
    path: '',
    component: MeetTypeComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'MeetTypes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MeetTypeDetailComponent,
    resolve: {
      meetType: MeetTypeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MeetTypes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MeetTypeUpdateComponent,
    resolve: {
      meetType: MeetTypeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MeetTypes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MeetTypeUpdateComponent,
    resolve: {
      meetType: MeetTypeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MeetTypes'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const meetTypePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MeetTypeDeletePopupComponent,
    resolve: {
      meetType: MeetTypeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MeetTypes'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
