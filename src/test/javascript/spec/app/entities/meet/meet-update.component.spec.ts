/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { MeetShowTestModule } from '../../../test.module';
import { MeetUpdateComponent } from 'app/entities/meet/meet-update.component';
import { MeetService } from 'app/entities/meet/meet.service';
import { Meet } from 'app/shared/model/meet.model';

describe('Component Tests', () => {
  describe('Meet Management Update Component', () => {
    let comp: MeetUpdateComponent;
    let fixture: ComponentFixture<MeetUpdateComponent>;
    let service: MeetService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MeetShowTestModule],
        declarations: [MeetUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MeetUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MeetUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MeetService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Meet(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Meet();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
