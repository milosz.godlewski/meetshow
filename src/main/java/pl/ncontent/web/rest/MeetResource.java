package pl.ncontent.web.rest;

import io.undertow.util.BadRequestException;
import pl.ncontent.service.MeetService;
import pl.ncontent.web.rest.errors.BadRequestAlertException;
import pl.ncontent.service.dto.MeetDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link pl.ncontent.domain.Meet}.
 */
@RestController
@RequestMapping("/api")
public class MeetResource {

    private final Logger log = LoggerFactory.getLogger(MeetResource.class);

    private static final String ENTITY_NAME = "meet";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MeetService meetService;

    public MeetResource(MeetService meetService) {
        this.meetService = meetService;
    }

    /**
     * {@code POST  /meets} : Create a new meet.
     *
     * @param meetDTO the meetDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new meetDTO, or with status {@code 400 (Bad Request)} if the meet has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/meets")
    public ResponseEntity<MeetDTO> createMeet(@Valid @RequestBody MeetDTO meetDTO) throws URISyntaxException, BadRequestException {
        log.debug("REST request to save Meet : {}", meetDTO);
        if (meetDTO.getId() != null) {
            throw new BadRequestAlertException("A new meet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MeetDTO result = meetService.save(meetDTO);
        return ResponseEntity.created(new URI("/api/meets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /meets} : Updates an existing meet.
     *
     * @param meetDTO the meetDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated meetDTO,
     * or with status {@code 400 (Bad Request)} if the meetDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the meetDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/meets")
    public ResponseEntity<MeetDTO> updateMeet(@Valid @RequestBody MeetDTO meetDTO) throws URISyntaxException {
        log.debug("REST request to update Meet : {}", meetDTO);
        if (meetDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MeetDTO result = meetService.save(meetDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, meetDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /meets} : get all the meets.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of meets in body.
     */
    @GetMapping("/meets")
    public ResponseEntity<List<MeetDTO>> getAllMeets(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Meets");
        Page<MeetDTO> page = meetService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /meets/:id} : get the "id" meet.
     *
     * @param id the id of the meetDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the meetDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/meets/{id}")
    public ResponseEntity<MeetDTO> getMeet(@PathVariable Long id) {
        log.debug("REST request to get Meet : {}", id);
        Optional<MeetDTO> meetDTO = meetService.findOne(id);
        return ResponseUtil.wrapOrNotFound(meetDTO);
    }

    /**
     * {@code DELETE  /meets/:id} : delete the "id" meet.
     *
     * @param id the id of the meetDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/meets/{id}")
    public ResponseEntity<Void> deleteMeet(@PathVariable Long id) {
        log.debug("REST request to delete Meet : {}", id);
        meetService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
