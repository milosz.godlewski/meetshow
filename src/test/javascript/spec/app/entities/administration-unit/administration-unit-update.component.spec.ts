/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { MeetShowTestModule } from '../../../test.module';
import { AdministrationUnitUpdateComponent } from 'app/entities/administration-unit/administration-unit-update.component';
import { AdministrationUnitService } from 'app/entities/administration-unit/administration-unit.service';
import { AdministrationUnit } from 'app/shared/model/administration-unit.model';

describe('Component Tests', () => {
  describe('AdministrationUnit Management Update Component', () => {
    let comp: AdministrationUnitUpdateComponent;
    let fixture: ComponentFixture<AdministrationUnitUpdateComponent>;
    let service: AdministrationUnitService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MeetShowTestModule],
        declarations: [AdministrationUnitUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(AdministrationUnitUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AdministrationUnitUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AdministrationUnitService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AdministrationUnit(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AdministrationUnit();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
