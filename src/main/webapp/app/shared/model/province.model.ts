export interface IProvince {
  id?: number;
  name?: string;
}

export class Province implements IProvince {
  constructor(public id?: number, public name?: string) {}
}
