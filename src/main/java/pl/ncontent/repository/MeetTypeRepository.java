package pl.ncontent.repository;

import pl.ncontent.domain.MeetType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MeetType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MeetTypeRepository extends JpaRepository<MeetType, Long> {

}
