package pl.ncontent.web.rest;

import pl.ncontent.MeetShowApp;
import pl.ncontent.domain.MeetType;
import pl.ncontent.repository.MeetTypeRepository;
import pl.ncontent.service.MeetTypeService;
import pl.ncontent.service.dto.MeetTypeDTO;
import pl.ncontent.service.mapper.MeetTypeMapper;
import pl.ncontent.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static pl.ncontent.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link MeetTypeResource} REST controller.
 */
@SpringBootTest(classes = MeetShowApp.class)
public class MeetTypeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private MeetTypeRepository meetTypeRepository;

    @Autowired
    private MeetTypeMapper meetTypeMapper;

    @Autowired
    private MeetTypeService meetTypeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMeetTypeMockMvc;

    private MeetType meetType;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MeetTypeResource meetTypeResource = new MeetTypeResource(meetTypeService);
        this.restMeetTypeMockMvc = MockMvcBuilders.standaloneSetup(meetTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MeetType createEntity(EntityManager em) {
        MeetType meetType = new MeetType()
            .name(DEFAULT_NAME);
        return meetType;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MeetType createUpdatedEntity(EntityManager em) {
        MeetType meetType = new MeetType()
            .name(UPDATED_NAME);
        return meetType;
    }

    @BeforeEach
    public void initTest() {
        meetType = createEntity(em);
    }

    @Test
    @Transactional
    public void createMeetType() throws Exception {
        int databaseSizeBeforeCreate = meetTypeRepository.findAll().size();

        // Create the MeetType
        MeetTypeDTO meetTypeDTO = meetTypeMapper.toDto(meetType);
        restMeetTypeMockMvc.perform(post("/api/meet-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(meetTypeDTO)))
            .andExpect(status().isCreated());

        // Validate the MeetType in the database
        List<MeetType> meetTypeList = meetTypeRepository.findAll();
        assertThat(meetTypeList).hasSize(databaseSizeBeforeCreate + 1);
        MeetType testMeetType = meetTypeList.get(meetTypeList.size() - 1);
        assertThat(testMeetType.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createMeetTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = meetTypeRepository.findAll().size();

        // Create the MeetType with an existing ID
        meetType.setId(1L);
        MeetTypeDTO meetTypeDTO = meetTypeMapper.toDto(meetType);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMeetTypeMockMvc.perform(post("/api/meet-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(meetTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MeetType in the database
        List<MeetType> meetTypeList = meetTypeRepository.findAll();
        assertThat(meetTypeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMeetTypes() throws Exception {
        // Initialize the database
        meetTypeRepository.saveAndFlush(meetType);

        // Get all the meetTypeList
        restMeetTypeMockMvc.perform(get("/api/meet-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(meetType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getMeetType() throws Exception {
        // Initialize the database
        meetTypeRepository.saveAndFlush(meetType);

        // Get the meetType
        restMeetTypeMockMvc.perform(get("/api/meet-types/{id}", meetType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(meetType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMeetType() throws Exception {
        // Get the meetType
        restMeetTypeMockMvc.perform(get("/api/meet-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMeetType() throws Exception {
        // Initialize the database
        meetTypeRepository.saveAndFlush(meetType);

        int databaseSizeBeforeUpdate = meetTypeRepository.findAll().size();

        // Update the meetType
        MeetType updatedMeetType = meetTypeRepository.findById(meetType.getId()).get();
        // Disconnect from session so that the updates on updatedMeetType are not directly saved in db
        em.detach(updatedMeetType);
        updatedMeetType
            .name(UPDATED_NAME);
        MeetTypeDTO meetTypeDTO = meetTypeMapper.toDto(updatedMeetType);

        restMeetTypeMockMvc.perform(put("/api/meet-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(meetTypeDTO)))
            .andExpect(status().isOk());

        // Validate the MeetType in the database
        List<MeetType> meetTypeList = meetTypeRepository.findAll();
        assertThat(meetTypeList).hasSize(databaseSizeBeforeUpdate);
        MeetType testMeetType = meetTypeList.get(meetTypeList.size() - 1);
        assertThat(testMeetType.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingMeetType() throws Exception {
        int databaseSizeBeforeUpdate = meetTypeRepository.findAll().size();

        // Create the MeetType
        MeetTypeDTO meetTypeDTO = meetTypeMapper.toDto(meetType);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMeetTypeMockMvc.perform(put("/api/meet-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(meetTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MeetType in the database
        List<MeetType> meetTypeList = meetTypeRepository.findAll();
        assertThat(meetTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMeetType() throws Exception {
        // Initialize the database
        meetTypeRepository.saveAndFlush(meetType);

        int databaseSizeBeforeDelete = meetTypeRepository.findAll().size();

        // Delete the meetType
        restMeetTypeMockMvc.perform(delete("/api/meet-types/{id}", meetType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<MeetType> meetTypeList = meetTypeRepository.findAll();
        assertThat(meetTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MeetType.class);
        MeetType meetType1 = new MeetType();
        meetType1.setId(1L);
        MeetType meetType2 = new MeetType();
        meetType2.setId(meetType1.getId());
        assertThat(meetType1).isEqualTo(meetType2);
        meetType2.setId(2L);
        assertThat(meetType1).isNotEqualTo(meetType2);
        meetType1.setId(null);
        assertThat(meetType1).isNotEqualTo(meetType2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MeetTypeDTO.class);
        MeetTypeDTO meetTypeDTO1 = new MeetTypeDTO();
        meetTypeDTO1.setId(1L);
        MeetTypeDTO meetTypeDTO2 = new MeetTypeDTO();
        assertThat(meetTypeDTO1).isNotEqualTo(meetTypeDTO2);
        meetTypeDTO2.setId(meetTypeDTO1.getId());
        assertThat(meetTypeDTO1).isEqualTo(meetTypeDTO2);
        meetTypeDTO2.setId(2L);
        assertThat(meetTypeDTO1).isNotEqualTo(meetTypeDTO2);
        meetTypeDTO1.setId(null);
        assertThat(meetTypeDTO1).isNotEqualTo(meetTypeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(meetTypeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(meetTypeMapper.fromId(null)).isNull();
    }
}
