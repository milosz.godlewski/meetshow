package pl.ncontent.service.mapper;

import pl.ncontent.domain.*;
import pl.ncontent.service.dto.PriorityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Priority} and its DTO {@link PriorityDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PriorityMapper extends EntityMapper<PriorityDTO, Priority> {


    @Mapping(target = "meets", ignore = true)
    Priority toEntity(PriorityDTO priorityDTO);

    default Priority fromId(Long id) {
        if (id == null) {
            return null;
        }
        Priority priority = new Priority();
        priority.setId(id);
        return priority;
    }
}
