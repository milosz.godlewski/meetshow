export interface IAdministrationUnit {
  id?: number;
  name?: string;
}

export class AdministrationUnit implements IAdministrationUnit {
  constructor(public id?: number, public name?: string) {}
}
