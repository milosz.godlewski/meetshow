import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { County } from 'app/shared/model/county.model';
import { CountyService } from './county.service';
import { CountyComponent } from './county.component';
import { CountyDetailComponent } from './county-detail.component';
import { CountyUpdateComponent } from './county-update.component';
import { CountyDeletePopupComponent } from './county-delete-dialog.component';
import { ICounty } from 'app/shared/model/county.model';

@Injectable({ providedIn: 'root' })
export class CountyResolve implements Resolve<ICounty> {
  constructor(private service: CountyService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICounty> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<County>) => response.ok),
        map((county: HttpResponse<County>) => county.body)
      );
    }
    return of(new County());
  }
}

export const countyRoute: Routes = [
  {
    path: '',
    component: CountyComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Counties'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CountyDetailComponent,
    resolve: {
      county: CountyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Counties'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CountyUpdateComponent,
    resolve: {
      county: CountyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Counties'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CountyUpdateComponent,
    resolve: {
      county: CountyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Counties'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const countyPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CountyDeletePopupComponent,
    resolve: {
      county: CountyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Counties'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
