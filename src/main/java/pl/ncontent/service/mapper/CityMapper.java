package pl.ncontent.service.mapper;

import com.raps.code.generate.ws.Miejscowosc;
import pl.ncontent.domain.*;
import pl.ncontent.service.dto.CityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link City} and its DTO {@link CityDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CityMapper extends EntityMapper<CityDTO, City> {

    default CityDTO fromWeb(Miejscowosc city) {
         CityDTO cityDTO = new CityDTO();
         cityDTO.setName(city.getMiejscNazwa());
         cityDTO.setCityId(city.getMiejscIIPId());
         cityDTO.setCityPn(city.getMiejscIIPPn());
         return cityDTO;
    }

    default City fromId(Long id) {
        if (id == null) {
            return null;
        }
        City city = new City();
        city.setId(id);
        return city;
    }
}
