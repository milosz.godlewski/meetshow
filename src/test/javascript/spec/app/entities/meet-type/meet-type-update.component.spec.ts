/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { MeetShowTestModule } from '../../../test.module';
import { MeetTypeUpdateComponent } from 'app/entities/meet-type/meet-type-update.component';
import { MeetTypeService } from 'app/entities/meet-type/meet-type.service';
import { MeetType } from 'app/shared/model/meet-type.model';

describe('Component Tests', () => {
  describe('MeetType Management Update Component', () => {
    let comp: MeetTypeUpdateComponent;
    let fixture: ComponentFixture<MeetTypeUpdateComponent>;
    let service: MeetTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MeetShowTestModule],
        declarations: [MeetTypeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MeetTypeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MeetTypeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MeetTypeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MeetType(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MeetType();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
