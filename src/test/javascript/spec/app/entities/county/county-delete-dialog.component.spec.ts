/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MeetShowTestModule } from '../../../test.module';
import { CountyDeleteDialogComponent } from 'app/entities/county/county-delete-dialog.component';
import { CountyService } from 'app/entities/county/county.service';

describe('Component Tests', () => {
  describe('County Management Delete Component', () => {
    let comp: CountyDeleteDialogComponent;
    let fixture: ComponentFixture<CountyDeleteDialogComponent>;
    let service: CountyService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MeetShowTestModule],
        declarations: [CountyDeleteDialogComponent]
      })
        .overrideTemplate(CountyDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CountyDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CountyService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
