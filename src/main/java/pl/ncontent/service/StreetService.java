package pl.ncontent.service;

import com.raps.code.generate.ws.SlnError;
import com.raps.code.generate.ws.SlnWebServiceService;
import pl.ncontent.domain.Street;
import pl.ncontent.repository.StreetRepository;
import pl.ncontent.service.dto.AdministrationUnitDTO;
import pl.ncontent.service.dto.CityDTO;
import pl.ncontent.service.dto.StreetDTO;
import pl.ncontent.service.mapper.StreetMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.ncontent.web.rest.errors.BadRequestAlertException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Street}.
 */
@Service
@Transactional
public class StreetService {

    private final Logger log = LoggerFactory.getLogger(StreetService.class);

    private final StreetMapper streetMapper;
    private CityService cityService;

    public StreetService(CityService cityService, StreetMapper streetMapper) {
        this.cityService = cityService;
        this.streetMapper = streetMapper;
    }

    public List<StreetDTO> getStreetsListInGivenCityName(String provinceName, String countyName, String communeName,
                                                       String cityName) {
        Optional<CityDTO> city =
            cityService.getCitiesListInGivenUnitName(provinceName, countyName, communeName)
            .stream()
            .filter(cityDTO -> Objects.equals(cityDTO.getName().toLowerCase(), cityName.toLowerCase())).findFirst();
        if (!city.isPresent()) {
            throw new BadRequestAlertException("Commune name not found", "VALIDATION", "400");
        }
        SlnWebServiceService gugikService = new SlnWebServiceService();
        List<StreetDTO> streetsListToReturn = new ArrayList<>();
        try {
            gugikService.getSlnWebServicePort()
                .pobierzUlice(city.get().getCityPn(), city.get().getCityId(), false)
                .getUlica()
                .stream()
                .filter(street -> street.getCyklZyciaDo() == null)          //Removing history records.
                .map(streetMapper::fromWeb)
                .forEach(streetsListToReturn::add);
        } catch (SlnError slnError) {
            throw new BadRequestAlertException("Error", "ERROR", "500");
        }
        return streetsListToReturn;
    }
}
