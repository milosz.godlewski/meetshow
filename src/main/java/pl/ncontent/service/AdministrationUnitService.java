package pl.ncontent.service;

import com.raps.code.generate.ws.JednAdm;
import com.raps.code.generate.ws.SlnError;
import com.raps.code.generate.ws.SlnWebServiceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.ncontent.domain.AdministrationUnit;
import pl.ncontent.service.dto.AdministrationUnitDTO;
import pl.ncontent.service.mapper.AdministrationUnitMapper;
import pl.ncontent.web.rest.errors.BadRequestAlertException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AdministrationUnit}.
 */
@Service
@Transactional
public class AdministrationUnitService {

    private final static String PROVINCE_NAME = "zachodniopomorskie";
    private final static String COMMUNE_NAME = "policki";

    private final Logger log = LoggerFactory.getLogger(AdministrationUnitService.class);
    private final AdministrationUnitMapper administrationUnitMapper;

    public AdministrationUnitService(AdministrationUnitMapper administrationUnitMapper) {
        this.administrationUnitMapper = administrationUnitMapper;
    }

    /**
     * Get county list
     *
     * @return county list.
     */
    public List<AdministrationUnitDTO> getProvinceList() {
        SlnWebServiceService gugikService = new SlnWebServiceService();
        List<AdministrationUnitDTO> countyListToReturn = new ArrayList<>();
        try {
            List<JednAdm> webProvinces = gugikService.getSlnWebServicePort().pobierzWojewodztwa().getJednAdm();
            webProvinces.stream()
                .map(administrationUnitMapper::fromWeb)
                .forEach(countyListToReturn::add);
        } catch (SlnError slnError) {
            throw new BadRequestAlertException("Error", "ERROR", "500");
        }
        return countyListToReturn;
    }


    public List<AdministrationUnitDTO> getCountiesList(String provinceName) {
        SlnWebServiceService gugikService = new SlnWebServiceService();
        List<AdministrationUnitDTO> countyListToReturn = new ArrayList<>();
        try {
            List<JednAdm> webProvinces = gugikService.getSlnWebServicePort().pobierzWojewodztwa().getJednAdm();
            Optional<JednAdm> county = webProvinces.stream()
                .filter(jednAdm -> Objects.equals(jednAdm.getWojNazwa().toLowerCase(), provinceName.toLowerCase()))
                .findFirst();
            if (!county.isPresent()) {
                throw new BadRequestAlertException("Province name not found", "VALIDATION", "400");
            }

            gugikService.getSlnWebServicePort()
                .pobierzPowiaty(county.get().getWojIIPPn(), county.get().getWojIIPId(), false).getJednAdm()
                .stream().map(administrationUnitMapper::fromWeb)
                .forEach(countyListToReturn::add);
        } catch (SlnError slnError) {
            throw new BadRequestAlertException("Error", "ERROR", "500");
        }
        return countyListToReturn;
    }

    public Optional<AdministrationUnitDTO> getCountyForGivenName(String provinceName, String countyName) {
        return getCountiesList(provinceName).stream()
            .filter(dto -> Objects.equals(countyName, dto.getName())).findFirst();
    }

    public List<AdministrationUnitDTO> getWestPomeranianCounties() {
        return getCountiesList(PROVINCE_NAME);
    }

    public List<AdministrationUnitDTO> getCommunesForGivenCountyName(String provinceName, String countyName) {
        SlnWebServiceService gugikService = new SlnWebServiceService();
        List<AdministrationUnitDTO> communeListToReturn = new ArrayList<>();
        try {
            List<JednAdm> webProvinces = gugikService.getSlnWebServicePort().pobierzWojewodztwa().getJednAdm();
            Optional<JednAdm> county = webProvinces.stream()
                .filter(jednAdm -> Objects.equals(jednAdm.getWojNazwa().toLowerCase(), provinceName.toLowerCase()))
                .findFirst();
            if (!county.isPresent()) {
                throw new BadRequestAlertException("Province name not found", "VALIDATION", "400");
            }

            Optional<JednAdm> province = gugikService.getSlnWebServicePort()
                .pobierzPowiaty(county.get().getWojIIPPn(), county.get().getWojIIPId(), false).getJednAdm()
                .stream()
                .filter(adm -> Objects.equals(adm.getPowNazwa().toLowerCase(), countyName.toLowerCase())).findFirst();
            if (!province.isPresent()) {
                throw new BadRequestAlertException("County name not found.", "VALIDATION", "400");
            }

            gugikService.getSlnWebServicePort()
                .pobierzGminy(province.get().getPowIIPPn(), province.get().getPowIIPId(), false).getJednAdm()
                .stream().map(administrationUnitMapper::fromWeb)
                .forEach(communeListToReturn::add);

        } catch (SlnError slnError) {
            throw new BadRequestAlertException("Error", "ERROR", "500");
        }

        return communeListToReturn;
    }

    public List<AdministrationUnitDTO> getCommunesOfPolice() {
        return getCommunesForGivenCountyName(PROVINCE_NAME, COMMUNE_NAME);
    }

    public Optional<AdministrationUnitDTO> getCommuneForGivenName(String provinceName, String countyName,
                                                                  String communeName) {
        return getCommunesForGivenCountyName(provinceName, countyName).stream()
            .filter(dto -> Objects.equals(communeName.toLowerCase(), dto.getName().toLowerCase())).findFirst();
    }

}
