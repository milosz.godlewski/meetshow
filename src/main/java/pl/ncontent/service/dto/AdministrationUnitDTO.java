package pl.ncontent.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link pl.ncontent.domain.AdministrationUnit} entity.
 */
public class AdministrationUnitDTO implements Serializable {

    private Long id;
    private String name;
    private String provincePn;
    private String provinceId;
    private String countyPn;
    private String countyId;
    private String communePn;
    private String communeId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvincePn() {
        return provincePn;
    }

    public void setProvincePn(String provincePn) {
        this.provincePn = provincePn;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getCountyPn() {
        return countyPn;
    }

    public void setCountyPn(String countyPn) {
        this.countyPn = countyPn;
    }

    public String getCountyId() {
        return countyId;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId;
    }

    public String getCommunePn() {
        return communePn;
    }

    public void setCommunePn(String communePn) {
        this.communePn = communePn;
    }

    public String getCommuneId() {
        return communeId;
    }

    public void setCommuneId(String communeId) {
        this.communeId = communeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdministrationUnitDTO that = (AdministrationUnitDTO) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(provincePn, that.provincePn) &&
            Objects.equals(provinceId, that.provinceId) &&
            Objects.equals(countyPn, that.countyPn) &&
            Objects.equals(countyId, that.countyId) &&
            Objects.equals(communePn, that.communePn) &&
            Objects.equals(communeId, that.communeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, provincePn, provinceId, countyPn, countyId, communePn, communeId);
    }

    @Override
    public String toString() {
        return "AdministrationUnitDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", provincePn='" + provincePn + '\'' +
            ", provinceId='" + provinceId + '\'' +
            ", countyPn='" + countyPn + '\'' +
            ", countyId='" + countyId + '\'' +
            ", communePn='" + communePn + '\'' +
            ", communeId='" + communeId + '\'' +
            '}';
    }
}
