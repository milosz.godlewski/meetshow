import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ICommune, Commune } from 'app/shared/model/commune.model';
import { CommuneService } from './commune.service';

@Component({
  selector: 'jhi-commune-update',
  templateUrl: './commune-update.component.html'
})
export class CommuneUpdateComponent implements OnInit {
  commune: ICommune;
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: []
  });

  constructor(protected communeService: CommuneService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ commune }) => {
      this.updateForm(commune);
      this.commune = commune;
    });
  }

  updateForm(commune: ICommune) {
    this.editForm.patchValue({
      id: commune.id,
      name: commune.name
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const commune = this.createFromForm();
    if (commune.id !== undefined) {
      this.subscribeToSaveResponse(this.communeService.update(commune));
    } else {
      this.subscribeToSaveResponse(this.communeService.create(commune));
    }
  }

  private createFromForm(): ICommune {
    const entity = {
      ...new Commune(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommune>>) {
    result.subscribe((res: HttpResponse<ICommune>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
