import { NgModule } from '@angular/core';

import { MeetShowSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [MeetShowSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [MeetShowSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class MeetShowSharedCommonModule {}
