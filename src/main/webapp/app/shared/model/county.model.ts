export interface ICounty {
  id?: number;
  name?: string;
}

export class County implements ICounty {
  constructor(public id?: number, public name?: string) {}
}
