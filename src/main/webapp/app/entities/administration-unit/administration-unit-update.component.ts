import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IAdministrationUnit, AdministrationUnit } from 'app/shared/model/administration-unit.model';
import { AdministrationUnitService } from './administration-unit.service';

@Component({
  selector: 'jhi-administration-unit-update',
  templateUrl: './administration-unit-update.component.html'
})
export class AdministrationUnitUpdateComponent implements OnInit {
  administrationUnit: IAdministrationUnit;
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: []
  });

  constructor(
    protected administrationUnitService: AdministrationUnitService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ administrationUnit }) => {
      this.updateForm(administrationUnit);
      this.administrationUnit = administrationUnit;
    });
  }

  updateForm(administrationUnit: IAdministrationUnit) {
    this.editForm.patchValue({
      id: administrationUnit.id,
      name: administrationUnit.name
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const administrationUnit = this.createFromForm();
    if (administrationUnit.id !== undefined) {
      this.subscribeToSaveResponse(this.administrationUnitService.update(administrationUnit));
    } else {
      this.subscribeToSaveResponse(this.administrationUnitService.create(administrationUnit));
    }
  }

  private createFromForm(): IAdministrationUnit {
    const entity = {
      ...new AdministrationUnit(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAdministrationUnit>>) {
    result.subscribe((res: HttpResponse<IAdministrationUnit>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
