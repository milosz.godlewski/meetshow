/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MeetShowTestModule } from '../../../test.module';
import { MeetTypeDetailComponent } from 'app/entities/meet-type/meet-type-detail.component';
import { MeetType } from 'app/shared/model/meet-type.model';

describe('Component Tests', () => {
  describe('MeetType Management Detail Component', () => {
    let comp: MeetTypeDetailComponent;
    let fixture: ComponentFixture<MeetTypeDetailComponent>;
    const route = ({ data: of({ meetType: new MeetType(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MeetShowTestModule],
        declarations: [MeetTypeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MeetTypeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MeetTypeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.meetType).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
