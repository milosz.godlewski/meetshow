import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICounty } from 'app/shared/model/county.model';

@Component({
  selector: 'jhi-county-detail',
  templateUrl: './county-detail.component.html'
})
export class CountyDetailComponent implements OnInit {
  county: ICounty;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ county }) => {
      this.county = county;
    });
  }

  previousState() {
    window.history.back();
  }
}
