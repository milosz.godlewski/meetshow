import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MeetShowSharedModule } from 'app/shared';
import {
  MeetTypeComponent,
  MeetTypeDetailComponent,
  MeetTypeUpdateComponent,
  MeetTypeDeletePopupComponent,
  MeetTypeDeleteDialogComponent,
  meetTypeRoute,
  meetTypePopupRoute
} from './';

const ENTITY_STATES = [...meetTypeRoute, ...meetTypePopupRoute];

@NgModule({
  imports: [MeetShowSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MeetTypeComponent,
    MeetTypeDetailComponent,
    MeetTypeUpdateComponent,
    MeetTypeDeleteDialogComponent,
    MeetTypeDeletePopupComponent
  ],
  entryComponents: [MeetTypeComponent, MeetTypeUpdateComponent, MeetTypeDeleteDialogComponent, MeetTypeDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MeetShowMeetTypeModule {}
