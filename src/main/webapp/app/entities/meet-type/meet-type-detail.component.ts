import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMeetType } from 'app/shared/model/meet-type.model';

@Component({
  selector: 'jhi-meet-type-detail',
  templateUrl: './meet-type-detail.component.html'
})
export class MeetTypeDetailComponent implements OnInit {
  meetType: IMeetType;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ meetType }) => {
      this.meetType = meetType;
    });
  }

  previousState() {
    window.history.back();
  }
}
