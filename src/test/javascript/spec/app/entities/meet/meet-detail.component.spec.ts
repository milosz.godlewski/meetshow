/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MeetShowTestModule } from '../../../test.module';
import { MeetDetailComponent } from 'app/entities/meet/meet-detail.component';
import { Meet } from 'app/shared/model/meet.model';

describe('Component Tests', () => {
  describe('Meet Management Detail Component', () => {
    let comp: MeetDetailComponent;
    let fixture: ComponentFixture<MeetDetailComponent>;
    const route = ({ data: of({ meet: new Meet(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MeetShowTestModule],
        declarations: [MeetDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MeetDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MeetDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.meet).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
