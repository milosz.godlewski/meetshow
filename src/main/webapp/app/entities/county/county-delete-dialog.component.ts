import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICounty } from 'app/shared/model/county.model';
import { CountyService } from './county.service';

@Component({
  selector: 'jhi-county-delete-dialog',
  templateUrl: './county-delete-dialog.component.html'
})
export class CountyDeleteDialogComponent {
  county: ICounty;

  constructor(protected countyService: CountyService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.countyService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'countyListModification',
        content: 'Deleted an county'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-county-delete-popup',
  template: ''
})
export class CountyDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ county }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CountyDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.county = county;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/county', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/county', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
