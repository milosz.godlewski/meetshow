package pl.ncontent.repository;

import pl.ncontent.domain.Meet;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;


/**
 * Spring Data  repository for the Meet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MeetRepository extends JpaRepository<Meet, Long> {

    @Query(value = "SELECT COUNT(id) FROM Meet m where m.starting_date >= ?1 AND m.ending_date <= ?2", nativeQuery = true)
    Integer getNumberOfMeetingsBetweenDates(ZonedDateTime startDate, ZonedDateTime endDate);
}
