import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAdministrationUnit } from 'app/shared/model/administration-unit.model';

@Component({
  selector: 'jhi-administration-unit-detail',
  templateUrl: './administration-unit-detail.component.html'
})
export class AdministrationUnitDetailComponent implements OnInit {
  administrationUnit: IAdministrationUnit;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ administrationUnit }) => {
      this.administrationUnit = administrationUnit;
    });
  }

  previousState() {
    window.history.back();
  }
}
