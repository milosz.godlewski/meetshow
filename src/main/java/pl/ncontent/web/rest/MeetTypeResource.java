package pl.ncontent.web.rest;

import pl.ncontent.service.MeetTypeService;
import pl.ncontent.web.rest.errors.BadRequestAlertException;
import pl.ncontent.service.dto.MeetTypeDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link pl.ncontent.domain.MeetType}.
 */
@RestController
@RequestMapping("/api")
public class MeetTypeResource {

    private final Logger log = LoggerFactory.getLogger(MeetTypeResource.class);

    private static final String ENTITY_NAME = "meetType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MeetTypeService meetTypeService;

    public MeetTypeResource(MeetTypeService meetTypeService) {
        this.meetTypeService = meetTypeService;
    }

    /**
     * {@code POST  /meet-types} : Create a new meetType.
     *
     * @param meetTypeDTO the meetTypeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new meetTypeDTO, or with status {@code 400 (Bad Request)} if the meetType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/meet-types")
    public ResponseEntity<MeetTypeDTO> createMeetType(@RequestBody MeetTypeDTO meetTypeDTO) throws URISyntaxException {
        log.debug("REST request to save MeetType : {}", meetTypeDTO);
        if (meetTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new meetType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MeetTypeDTO result = meetTypeService.save(meetTypeDTO);
        return ResponseEntity.created(new URI("/api/meet-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /meet-types} : Updates an existing meetType.
     *
     * @param meetTypeDTO the meetTypeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated meetTypeDTO,
     * or with status {@code 400 (Bad Request)} if the meetTypeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the meetTypeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/meet-types")
    public ResponseEntity<MeetTypeDTO> updateMeetType(@RequestBody MeetTypeDTO meetTypeDTO) throws URISyntaxException {
        log.debug("REST request to update MeetType : {}", meetTypeDTO);
        if (meetTypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MeetTypeDTO result = meetTypeService.save(meetTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, meetTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /meet-types} : get all the meetTypes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of meetTypes in body.
     */
    @GetMapping("/meet-types")
    public ResponseEntity<List<MeetTypeDTO>> getAllMeetTypes(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of MeetTypes");
        Page<MeetTypeDTO> page = meetTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /meet-types/:id} : get the "id" meetType.
     *
     * @param id the id of the meetTypeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the meetTypeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/meet-types/{id}")
    public ResponseEntity<MeetTypeDTO> getMeetType(@PathVariable Long id) {
        log.debug("REST request to get MeetType : {}", id);
        Optional<MeetTypeDTO> meetTypeDTO = meetTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(meetTypeDTO);
    }

    /**
     * {@code DELETE  /meet-types/:id} : delete the "id" meetType.
     *
     * @param id the id of the meetTypeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/meet-types/{id}")
    public ResponseEntity<Void> deleteMeetType(@PathVariable Long id) {
        log.debug("REST request to delete MeetType : {}", id);
        meetTypeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
