import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IStreet, Street } from 'app/shared/model/street.model';
import { StreetService } from './street.service';

@Component({
  selector: 'jhi-street-update',
  templateUrl: './street-update.component.html'
})
export class StreetUpdateComponent implements OnInit {
  street: IStreet;
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: []
  });

  constructor(protected streetService: StreetService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ street }) => {
      this.updateForm(street);
      this.street = street;
    });
  }

  updateForm(street: IStreet) {
    this.editForm.patchValue({
      id: street.id,
      name: street.name
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const street = this.createFromForm();
    if (street.id !== undefined) {
      this.subscribeToSaveResponse(this.streetService.update(street));
    } else {
      this.subscribeToSaveResponse(this.streetService.create(street));
    }
  }

  private createFromForm(): IStreet {
    const entity = {
      ...new Street(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStreet>>) {
    result.subscribe((res: HttpResponse<IStreet>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
