/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MeetShowTestModule } from '../../../test.module';
import { MeetDeleteDialogComponent } from 'app/entities/meet/meet-delete-dialog.component';
import { MeetService } from 'app/entities/meet/meet.service';

describe('Component Tests', () => {
  describe('Meet Management Delete Component', () => {
    let comp: MeetDeleteDialogComponent;
    let fixture: ComponentFixture<MeetDeleteDialogComponent>;
    let service: MeetService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MeetShowTestModule],
        declarations: [MeetDeleteDialogComponent]
      })
        .overrideTemplate(MeetDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MeetDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MeetService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
