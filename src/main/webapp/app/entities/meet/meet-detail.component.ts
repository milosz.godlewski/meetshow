import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMeet } from 'app/shared/model/meet.model';

@Component({
  selector: 'jhi-meet-detail',
  templateUrl: './meet-detail.component.html'
})
export class MeetDetailComponent implements OnInit {
  meet: IMeet;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ meet }) => {
      this.meet = meet;
    });
  }

  previousState() {
    window.history.back();
  }
}
