package pl.ncontent.service.mapper;

import com.raps.code.generate.ws.Ulica;
import org.apache.commons.lang3.StringUtils;
import pl.ncontent.domain.*;
import pl.ncontent.service.dto.StreetDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Street} and its DTO {@link StreetDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface StreetMapper extends EntityMapper<StreetDTO, Street> {
    default Street fromId(Long id) {
        if (id == null) {
            return null;
        }
        Street street = new Street();
        street.setId(id);
        return street;
    }

    default StreetDTO fromWeb(Ulica street) {
        StreetDTO streetDTO = new StreetDTO();
        streetDTO.setName(StringUtils.join(new String[]
            {street.getUlNazwaPrzed1(),street.getUlNazwaPrzed2(),street.getUlNazwaCzesc(),street.getUlNazwaGlowna()}
            , " "));
        return streetDTO;
    }
}
