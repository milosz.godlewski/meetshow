package pl.ncontent.service;

import pl.ncontent.domain.MeetType;
import pl.ncontent.repository.MeetTypeRepository;
import pl.ncontent.service.dto.MeetTypeDTO;
import pl.ncontent.service.mapper.MeetTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link MeetType}.
 */
@Service
@Transactional
public class MeetTypeService {

    private final Logger log = LoggerFactory.getLogger(MeetTypeService.class);

    private final MeetTypeRepository meetTypeRepository;

    private final MeetTypeMapper meetTypeMapper;

    public MeetTypeService(MeetTypeRepository meetTypeRepository, MeetTypeMapper meetTypeMapper) {
        this.meetTypeRepository = meetTypeRepository;
        this.meetTypeMapper = meetTypeMapper;
    }

    /**
     * Save a meetType.
     *
     * @param meetTypeDTO the entity to save.
     * @return the persisted entity.
     */
    public MeetTypeDTO save(MeetTypeDTO meetTypeDTO) {
        log.debug("Request to save MeetType : {}", meetTypeDTO);
        MeetType meetType = meetTypeMapper.toEntity(meetTypeDTO);
        meetType = meetTypeRepository.save(meetType);
        return meetTypeMapper.toDto(meetType);
    }

    /**
     * Get all the meetTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MeetTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MeetTypes");
        return meetTypeRepository.findAll(pageable)
            .map(meetTypeMapper::toDto);
    }


    /**
     * Get one meetType by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MeetTypeDTO> findOne(Long id) {
        log.debug("Request to get MeetType : {}", id);
        return meetTypeRepository.findById(id)
            .map(meetTypeMapper::toDto);
    }

    /**
     * Delete the meetType by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MeetType : {}", id);
        meetTypeRepository.deleteById(id);
    }
}
