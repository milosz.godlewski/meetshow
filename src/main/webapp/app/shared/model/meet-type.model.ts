import { IMeet } from 'app/shared/model/meet.model';

export interface IMeetType {
  id?: number;
  name?: string;
  meets?: IMeet[];
}

export class MeetType implements IMeetType {
  constructor(public id?: number, public name?: string, public meets?: IMeet[]) {}
}
